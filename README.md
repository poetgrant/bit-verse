Bit Verse v0.01
author: Grant Ford "aka PoetGrant"
date: 20 Sep 2018
license: Creative Commons

Bit Verse is something that has been brewing in my mind for a couple of years now and this is my attempt to make it reality.

The first type of Bit Verse that I am working on is called "8-bit-sonnet".

8-bit-sonnet

- 8 lines
- 8 syllables
- i/o rhyming scheme

i/o rhyming scheme:

(this rhyming scheme will be detailed in the wiki)